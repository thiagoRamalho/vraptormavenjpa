package br.com.vraptormavenjpa.config;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.vraptormavenjpa.modelo.Nivel;
import br.com.vraptormavenjpa.modelo.Usuario;

public class Main {

	public static void main(String[] args) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("default");
		EntityManager entityManager = factory.createEntityManager();

		try{

			entityManager.getTransaction().begin();

			String hql = " delete from Usuario u where u.nickName in :lista";

			Query query = entityManager.createQuery(hql);

			query.setParameter("lista", Arrays.asList("cliente", "fornecedor"));

			query.executeUpdate();
				
			Usuario usuario1 = new Usuario(null, "comum", "comum", Nivel.COMUM);

			Usuario usuario2 = new Usuario(null, "admin", "admin", Nivel.ADMINISTRADOR);
			
			entityManager.persist(usuario1);
			entityManager.persist(usuario2);

			entityManager.getTransaction().commit();
			
			entityManager.getTransaction().begin();

			TypedQuery<Usuario> q = entityManager.createQuery("from Usuario",Usuario.class);
			
			List<Usuario> resultList = q.getResultList();
			
			for (Usuario usuario : resultList) {
				System.out.println(usuario.getNickName());
			}
			
			entityManager.close();

			factory.close();

		}catch(Exception e){
			e.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}
}
