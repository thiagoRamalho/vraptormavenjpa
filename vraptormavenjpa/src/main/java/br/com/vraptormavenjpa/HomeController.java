package br.com.vraptormavenjpa;

import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.vraptormavenjpa.dao.UsuarioDAO;
import br.com.vraptormavenjpa.modelo.Nivel;
import br.com.vraptormavenjpa.modelo.Usuario;

@Resource
public class HomeController {

	private Result result;
	private UsuarioDAO usuarioDAO;
	
	public HomeController(Result result, UsuarioDAO usuarioDAO) {
		super();
		this.result = result;
		this.usuarioDAO = usuarioDAO;
	}

	@Get("/home")
	public void home(){
		List<Usuario> lista = this.usuarioDAO.buscarTodos();
		this.result.include("lista",lista);
	}
	
	@Get("/")
	public void index(){
		
		for (int i = 0; i < 10; i++) {
			
			 Nivel nivel = i%2 == 0 ? Nivel.ADMINISTRADOR  : Nivel.COMUM;
			
			this.usuarioDAO.inserir(new Usuario(null, "nickName"+i, "senha",nivel));
		}
		
		this.result.redirectTo(this).home();
	}
}
