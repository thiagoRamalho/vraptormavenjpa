package br.com.vraptormavenjpa.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Usuario {

	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	private String nickName;
	private String senha;
	private Nivel nivel;
	
	protected Usuario(){}
	
	public Usuario(Long id, String nickName, String senha, Nivel nivel) {
		super();
		this.id = id;
		this.nickName = nickName;
		this.senha = senha;
		this.nivel = nivel;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getNickName() {
		return nickName;
	}
	
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getSenha() {
		return senha;
	}
	public Nivel getNivel() {
		return nivel;
	}
	
	@Override
	public Usuario clone() {
		return new Usuario(id, nickName, senha, nivel);
	}
}
