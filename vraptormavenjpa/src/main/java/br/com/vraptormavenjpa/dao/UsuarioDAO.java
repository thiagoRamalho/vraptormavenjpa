package br.com.vraptormavenjpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.Resource;
import br.com.vraptormavenjpa.modelo.Usuario;

@Resource
public class UsuarioDAO {
	
	EntityManager entityManager;

	public UsuarioDAO(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	public Usuario buscarPor(String nick, String senha){
		
		String hql = " from Usuario u where u.nickName = :nick and u.senha = :senha";
		
		TypedQuery<Usuario> query = this.entityManager.createQuery(hql, Usuario.class);
		
		query.setParameter("nick", nick);
		query.setParameter("senha", senha);
		
		List<Usuario> resultList = query.getResultList();
		
		return resultList.isEmpty() ? null : resultList.get(0);
	}

	public Usuario inserir(Usuario usuario) {
		this.entityManager.persist(usuario);
		return usuario;
	}

	public List<Usuario> buscarTodos() {
		
		String hql = " from Usuario";
		
		TypedQuery<Usuario> query = this.entityManager.createQuery(hql, Usuario.class);
		
		return query.getResultList();
	}
}
